//Jose Carlos Betancourt Saiz de la Mora 1935788

package movies.importer;
import java.util.ArrayList;

public class LowercaseProcessor extends Processor {
	
	LowercaseProcessor(String srcDir, String outputDir){
		super(srcDir, outputDir, true);
	}
	
	public ArrayList<String> process(ArrayList<String> arr_list){
		ArrayList<String> asLower = new ArrayList<String>();
		
		for (int i=0; i<arr_list.size(); i++) {
			String inLower = arr_list.get(i).toLowerCase();
			asLower.add(inLower);
		}
		
		return asLower;
	}

}
