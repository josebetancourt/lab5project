//Jose Carlos Betancourt Saiz de la Mora 1935788

package movies.importer;

import java.io.IOException;

public class ProcessingTest {

	public static void main(String[] args) throws IOException {
		
		String srcDir = "C:\\JOSE\\Dawson\\Semester 3\\Java\\java310\\lab5project";
		String outputDir = "C:\\JOSE\\Dawson\\Semester 3\\Java\\java310\\output";
		
		LowercaseProcessor intoLower = new LowercaseProcessor(srcDir, outputDir);
		
		intoLower.execute();

		RemoveDuplicates notRepeated = new RemoveDuplicates(outputDir, srcDir);
		
		notRepeated.execute();
	}

}
