//Jose Carlos Betancourt Saiz de la Mora 1935788

package movies.importer;
import java.util.ArrayList;

public class RemoveDuplicates extends Processor{
	
	RemoveDuplicates(String srcDir, String outputDir){
		super(srcDir, outputDir, false);
	}
	
	public ArrayList<String> process(ArrayList<String> arr_list){
		ArrayList<String> asOnce = new ArrayList<String>();
		
		for (int i=0; i<arr_list.size(); i++) {
			boolean repeated = asOnce.contains(arr_list.get(i));
			if (!repeated) {
				asOnce.add(arr_list.get(i));
			}
		}
		
		return asOnce;
	}

}
